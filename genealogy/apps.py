from django.apps import AppConfig


class GenealogyConfig(AppConfig):
    name = 'genealogy'
