from django.contrib import admin
from .models import (
    Person,
    Union,
)
# Register your models here.



@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    pass


@admin.register(Union)
class UnionAdmin(admin.ModelAdmin):
    pass