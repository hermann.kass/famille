from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.


class Person(models.Model):
    mother = models.ForeignKey("Person", verbose_name=_("Mother"), on_delete=models.CASCADE, blank=True, null=True, related_name="mother_child")
    father = models.ForeignKey("Person", verbose_name=_("Father"), on_delete=models.CASCADE, blank=True, null=True, related_name="father_child")
    firstname = models.CharField(_("Firstname"), max_length=50, blank=True, null=True)
    lastname = models.CharField(_("Lastname"), max_length=50, blank=True, null=True)
    sex = models.CharField(_("Sex"), max_length=50, blank=True, null=True)
    #picture = models.ImageField()
    birthdate = models.CharField(_("Birthdate"), max_length=50, blank=True, null=True)
    birth_note = models.CharField(_("Birth note"), max_length=50, blank=True, null=True)
    birth_place = models.CharField(_("Birth place"), max_length=50, blank=True, null=True)
    death_date = models.CharField(_("Death date"), max_length=50, blank=True, null=True)
    death_place = models.CharField(_("Death place"), max_length=50, blank=True, null=True)
    death_note = models.CharField(_("Death note"), max_length=50, blank=True, null=True)
    some_infos = models.CharField(_("Additional infos"), max_length=50, blank=True, null=True)
    

    class Meta:
        verbose_name = _("Person")
        verbose_name_plural = _("Persons")
    
    def __str__(self):
        return self.firstname, self.lastname

    def get_absolute_url(self):
        return reverse("person", kwargs={"pk": self.pk})



class Union(models.Model):
    male = models.ForeignKey(Person, verbose_name=_("Mother"), on_delete=models.CASCADE, blank=True, null=True, related_name="union_male")
    female = models.ForeignKey(Person, verbose_name=_("Mother"), on_delete=models.CASCADE, blank=True, null=True, related_name="union_female")
    start_date = models.DateField(_("Start date"), auto_now=False, auto_now_add=False, blank=True, null=True)
    end_date = models.DateField(_("End date"), auto_now=False, auto_now_add=False, blank=True, null=True)
    note = models.CharField(_("Note"), max_length=50, blank=True, null=True)

    class Meta:
        verbose_name = _("Union")
        verbose_name_plural = _("Unions")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Union_detail", kwargs={"pk": self.pk})
