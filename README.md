# famille
Ce projet se veut la mémoire de notre communauté. 


## Objectifs
- connaître ses proches
- connaîtres l'histoire de la famille
- aider les gens à identifier ou retrouver leurs familles d'origine
- retrouver son identité
- créer une communauté de personnes autour d'un objectif unique 

## Libre expression autour du projet
@hermann.kass : 
Il est important de connaître les membres de sa famille, cela aide très important dans le sens où il peut aider à l'épanouissement d'une personne de connaître un proche et son histoire.

## Quelques règles 
- savoir utiliser gitLab
- savoir programmer en python
- savoir virtualenv ou pipenv
- savoir programmer en python/Django
    - templatage
    - Class View
    - I18n

- savoir programmer en JavaScript
- avoir de l'imagination
- faire régulièrement des commits afin que nous puissions suivre l'évolution du travail

## Modélisation
### Tables
---
Person   
> mother  
> father  
> firstname  
> lastname  
> sexe  
> birthdate  
> birth_note  
> birth_place  
> death_date  
> death_place  
> death_note  
> some_infos


Union
> male  
> female  
> start_date  
> end_date  
> note
